﻿using System;
using System.Threading.Tasks;
using CoreGraphics;
using TicTacToe.Game;
using TicTacToe.Game.ResultChecker;
using TicTacToe.Marks;
using TicTacToe.Players;
using TicTacToe.UI;
using UIKit;

namespace TicTacToe
{
    public partial class RootViewController : GradientViewController
    {
        #region Fields
        private UIButton _startButton;
        private UILabel _logoLabel;
        private GameGridView _gameView;
        private ClassicGame _game;
        #endregion

        #region Constructors
        public RootViewController( IntPtr handle ) : base( handle )
        {
        }
        #endregion

        #region Properties
        private static bool UserInterfaceIdiomIsPhone
        {
            get
            {
                return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;
            }
        }
        #endregion

        #region View lifecycle
        private void StartButtonOnTouchUpInside( object sender, EventArgs eventArgs )
        {
            UIView.Animate( 0.5f,
                            () =>
                            {
                                _startButton.Center = new CGPoint( _startButton.Center.X, 2 * View.Bounds.Height );
                            },
                            () =>
                            {
                                _startButton.Hidden = true;
                                StartGame();
                            } );
        }

        private void StartGame()
        {
            _game = new ClassicGame( new HumanPlayer( "Player 1", new CrossMark( 0 ) ), new HumanPlayer( "Player 2", new NoughtMark( 1 ) ) );
            _game.StartGame();

            var offset = 10;
            var bottomMargin = 20;

            var gameDimensions = Math.Min( View.Frame.Width, View.Frame.Height ) - _logoLabel.Frame.Bottom - bottomMargin;

            _gameView = new GameGridView( 2, 2 );

            View.AddSubview( _gameView );

            _gameView.TranslatesAutoresizingMaskIntoConstraints = false;

            LayoutUtils.CenterHorizontally( View, _gameView );
            LayoutUtils.TopPosition( View, _gameView, _logoLabel, 20f );
            LayoutUtils.SetWidth( View, _gameView, (nfloat)gameDimensions );
            LayoutUtils.SetHeight( View, _gameView, (nfloat)gameDimensions );

            _gameView.Hidden = true;
            View.AddSubview( _gameView );

            var touchRecognizer = new UITapGestureRecognizer( recognizer =>
                                                              {
                                                                  if ( recognizer.State == UIGestureRecognizerState.Recognized )
                                                                  {
                                                                      var point = recognizer.LocationInView( _gameView );

                                                                      var coord = _gameView.GetTouchGridCoordinate( point );

                                                                      GridClick( coord );
                                                                  }
                                                              } );

            touchRecognizer.NumberOfTapsRequired = 1;
            touchRecognizer.NumberOfTouchesRequired = 1;

            _gameView.AddGestureRecognizer( touchRecognizer );

            _gameView.Hidden = false;
            _gameView.AnimateGrid();
        }

        public override void ViewWillAppear( bool animated )
        {
            base.ViewWillAppear( animated );

            View.TranslatesAutoresizingMaskIntoConstraints = false;

            _startButton = new UIButton();
            _startButton.SetTitle( "Start", UIControlState.Normal );
            _startButton.SetTitleColor( LayoutConstants.BlueColor, UIControlState.Normal );

            _startButton.TitleLabel.ShadowOffset = new CGSize( 4, 4 );
            _startButton.TitleLabel.ShadowColor = UIColor.White;
            _startButton.TitleLabel.Font = LayoutUtils.GetAppFontBold( 50 );
            _startButton.TouchUpInside += StartButtonOnTouchUpInside;
            _startButton.SizeToFit();
            _startButton.TranslatesAutoresizingMaskIntoConstraints = false;
            LayoutUtils.CenterView( View, _startButton );
            View.AddSubview( _startButton );

            _logoLabel = new UILabel();
            _logoLabel.Text = "Tic Tac Toe";
            _logoLabel.Font = LayoutUtils.GetAppFont( 35 );
            _logoLabel.TextColor = LayoutConstants.BrownLight;
            _logoLabel.SizeToFit();
            //_logoLabel.Center = new CGPoint( View.Frame.Width / 2, 60 );
            _logoLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddSubview( _logoLabel );
            LayoutUtils.CenterHorizontally( View, _logoLabel );
            LayoutUtils.TopPosition( View, _logoLabel, 60f );
        }

        public async void GridClick( GridCoordinate coordinate )
        {
            var player = _game.CurrentPlayer;

            var result = _game.CellClick( coordinate.X, coordinate.Y );

            if ( !result.NotAllowed )
            {
                _gameView.SetMark( coordinate, player.Mark );
            }

            if ( result.GameIsFinished )
            {
                _gameView.UserInteractionEnabled = false;

                if ( result.CheckResult.IsDraw )
                {
                    var alert = new UIAlertView( "Game Over", "DRAW GAME", null, "OK", null );
                    alert.Show();
                }
                else
                {
                    await Task.Delay( 400 );
                    _gameView.WinningLine( result.CheckResult.BeginningWinGridCoordinate, result.CheckResult.EndingWinGridCoordinate );
                    await Task.Delay( 1500 );
                    var alert = new UIAlertView( "Game Over", player.Name + " WIN", null, "OK", null );
                    alert.Show();
                }
                StopGame();
            }
        }

        public void StopGame()
        {
            _game = null;
            _gameView.RemoveFromSuperview();

            _startButton.Hidden = false;
        }

        /// <param name="toInterfaceOrientation">
        ///     <para>
        ///         The new orientation.
        ///     </para>
        /// </param>
        /// <param name="duration">
        ///     <para>
        ///         The duration of the rotation animation, in seconds.
        ///     </para>
        /// </param>
        /// <summary>
        ///     Called prior to a one-step interface rotation.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is called from with the animation block associated with a user interface rotation. When this method
        ///         is called, the <see cref="P:UIKit.UIViewController.InterfaceOrientation" /> is set to the new interface
        ///         orientation and the <see cref="P:UIKit.UIView.Bounds" /> of the <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> have been modified.
        ///     </para>
        /// </remarks>
        public override void WillAnimateRotation( UIInterfaceOrientation toInterfaceOrientation, double duration )
        {
            base.WillAnimateRotation( toInterfaceOrientation, duration );
        }

        public override void ViewDidAppear( bool animated )
        {
            base.ViewDidAppear( animated );
        }

        public override void ViewWillDisappear( bool animated )
        {
            base.ViewWillDisappear( animated );
        }

        public override void ViewDidDisappear( bool animated )
        {
            base.ViewDidDisappear( animated );
        }
        #endregion
    }
}
