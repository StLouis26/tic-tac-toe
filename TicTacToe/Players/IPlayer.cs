using TicTacToe.Marks;

namespace TicTacToe.Players
{
    internal interface IPlayer
    {
        #region Properties
        string Name
        {
            get;
        }

        /// <summary>
        ///     true - if user is real player and cell click is required. false - for AI mostly
        /// </summary>
        bool RequiresInput
        {
            get;
        }

        IMark Mark
        {
            get;
        }
        #endregion
    }
}
