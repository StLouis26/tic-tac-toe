using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using TicTacToe.Marks;
using UIKit;

namespace TicTacToe.Players
{
    class HumanPlayer : IPlayer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public HumanPlayer(string name, IMark mark)
        {
            if ( string.IsNullOrEmpty( name ) )
            {
                throw new NullReferenceException("Player name cannot be empty");
            }

            if ( mark == null )
            {
                throw new NullReferenceException("Player mark is null");
            }

            Name = name;
            Mark = mark;
        }

        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// true - if user is real player and cell click is required. false - for AI mostly
        /// </summary>
        public bool RequiresInput
        {
            get
            {
                return true;
            }
        }

        public IMark Mark
        {
            get;
            private set;
        }
    }
}