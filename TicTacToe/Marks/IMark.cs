namespace TicTacToe.Marks
{
    internal interface IMark
    {
        #region Properties
        /// <summary>
        ///     Id for one team of players
        /// </summary>
        int PlayerGroupId
        {
            get;
        }

        /// <summary>
        ///     Neutral mark
        /// </summary>
        bool IsEmptyMark
        {
            get;
        }
        #endregion
    }
}
