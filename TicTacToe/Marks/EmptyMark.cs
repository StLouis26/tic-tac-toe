using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace TicTacToe.Marks
{
    class EmptyMark : IMark
    {
        /// <summary>
        ///     Id for one team of players
        /// </summary>
        public int PlayerGroupId
        {
            get
            {
                return -1;
            }
        }

        /// <summary>
        ///     Neutral mark
        /// </summary>
        public bool IsEmptyMark
        {
            get
            {
                return true;
            }
        }
    }
}