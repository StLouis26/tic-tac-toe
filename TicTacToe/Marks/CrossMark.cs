using System;

namespace TicTacToe.Marks
{
    internal class CrossMark : IMark
    {
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public CrossMark( int playerGroupId )
        {
            PlayerGroupId = playerGroupId;

            if ( playerGroupId < 0 )
            {
                throw new ArgumentException();
            }
        }
        #endregion

        #region Properties
        /// <summary>
        ///     Id for one team of players
        /// </summary>
        public int PlayerGroupId
        {
            get;
            private set;
        }

        /// <summary>
        ///     Neutral mark
        /// </summary>
        public bool IsEmptyMark
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}
