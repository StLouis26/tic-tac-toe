namespace TicTacToe.Marks
{
    internal class NoughtMark : IMark
    {
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public NoughtMark( int playerGroupId )
        {
            PlayerGroupId = playerGroupId;
        }
        #endregion

        #region Properties
        /// <summary>
        ///     Id for one team of players
        /// </summary>
        public int PlayerGroupId
        {
            get;
            private set;
        }

        /// <summary>
        ///     Neutral mark
        /// </summary>
        public bool IsEmptyMark
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}
