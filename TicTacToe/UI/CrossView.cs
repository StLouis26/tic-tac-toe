using System;
using System.Threading.Tasks;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace TicTacToe.UI
{
    internal class CrossView : UIView
    {
        #region Fields
        private CAShapeLayer _layerLeftTop;
        private CAShapeLayer _layerRightTop;
        #endregion

        #region Constructors
        public CrossView()
        {
            _layerLeftTop = new CAShapeLayer();
            _layerLeftTop.StrokeColor = UIColor.Green.CGColor;
            _layerLeftTop.LineWidth = 4f;
            _layerLeftTop.LineCap = CAShapeLayer.CapRound;

            _layerRightTop = new CAShapeLayer();
            _layerRightTop.StrokeColor = UIColor.Green.CGColor;
            _layerRightTop.LineWidth = 4f;
            _layerRightTop.LineCap = CAShapeLayer.CapRound;

            Layer.AddSublayer( _layerRightTop );
            Layer.AddSublayer( _layerLeftTop );
        }
        #endregion

        #region Public methods
        [Export( "layerClass" )]
        public static Class LayerClass()
        {
            return new Class( typeof ( CAShapeLayer ) );
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var inset = 11f;
            _layerLeftTop.Path = LayerUtils.CreateLineLayerBezierPath( new CGPoint( inset, inset ), new CGPoint( Bounds.Width - inset, Bounds.Height - inset ) );
            _layerRightTop.Path = LayerUtils.CreateLineLayerBezierPath( new CGPoint( Bounds.Width - inset, inset ), new CGPoint( inset, Bounds.Height - inset ) );
        }

        public async void Animate()
        {
            var animation = CABasicAnimation.FromKeyPath( @"strokeEnd" );

            _layerLeftTop.StrokeEnd = 0.0f;
            _layerRightTop.StrokeEnd = 0.0f;

            animation.From = FromObject( (nfloat)0.0 );
            animation.To = FromObject( (nfloat)1.0 );
            animation.RemovedOnCompletion = false;
            animation.Duration = 0.4f;
            animation.TimingFunction = CAMediaTimingFunction.FromName( CAMediaTimingFunction.EaseInEaseOut );

            // _horizLayers[i].StrokeEnd = 1.0f;
            _layerLeftTop.AddAnimation( animation, animation.KeyPath );
            _layerLeftTop.StrokeEnd = 1.0f;
            await Task.Delay( 400 );
            _layerRightTop.AddAnimation( animation, animation.KeyPath );
            _layerRightTop.StrokeEnd = 1.0f;
        }
        #endregion
    }
}
