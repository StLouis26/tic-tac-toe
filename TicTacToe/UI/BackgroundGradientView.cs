using CoreAnimation;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace TicTacToe.UI
{
    internal class BackgroundGradientView : UIView
    {
        #region Public methods
        [Export( "layerClass" )]
        public static Class LayerClass()
        {
            return new Class( typeof ( CAGradientLayer ) );
        }

        /// <summary>
        ///     Lays out subviews.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         The default implementation of this method does nothing on iOS 5.1 and earlier. Otherwise, the default
        ///         implementation uses any constraints you have set to determine the size and position of any subviews.
        ///     </para>
        ///     <para>
        ///         Subclasses can override this method as needed to perform more precise layout of their subviews. You should
        ///         override this method only if the autoresizing and constraint-based behaviors of the subviews do not offer the
        ///         behavior you want. You can use your implementation to set the frame rectangles of your subviews directly.
        ///     </para>
        ///     <para>
        ///         You should not call this method directly. If you want to force a layout update, call the setNeedsLayout method
        ///         instead to do so prior to the next drawing update. If you want to update the layout of your views immediately,
        ///         call the layoutIfNeeded method.
        ///     </para>
        /// </remarks>
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var layer = (CAGradientLayer)Layer;

            // var color1 = UIColor.FromRGB( 94, 41, 19 );
            var color1 = UIColor.FromRGB( 67, 30, 14 );
            var color2 = UIColor.FromRGB( 21, 10, 4 );

            var locations = new[]
                            {
                                NSNumber.FromFloat( 0.0f ),
                                //NSNumber.FromFloat( 0.02f ),
                                //NSNumber.FromFloat( 0.99f ),
                                NSNumber.FromFloat( 1.0f )
                            };

            layer.Colors = new[]
                           {
                               color1.CGColor,
                               color2.CGColor
                               //color3.CGColor,
                               //color4.CGColor
                           };
            layer.Locations = locations;
        }
        #endregion
    }
}
