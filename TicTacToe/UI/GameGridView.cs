using System;
using System.Drawing;
using System.Threading.Tasks;
using CoreAnimation;
using CoreGraphics;
using TicTacToe.Game.ResultChecker;
using TicTacToe.Marks;
using UIKit;

namespace TicTacToe.UI
{
    internal class GameGridView : UIView
    {
        #region Fields
        private CAShapeLayer[] _horizLayers;
        private CAShapeLayer[] _vertLayers;
        private nfloat _lineWidth = 4;
        private float _vertMargin, _horizMargin;
        #endregion

        #region Constructors
        public GameGridView( int horizLayersNumber, int vertLayersNumber)
        {
            _horizLayers = new CAShapeLayer[horizLayersNumber];
           

          
            for ( int i = 0; i < horizLayersNumber; i++ )
            {
                var layer = LayerUtils.CreateLineLayer( LayoutConstants.BrownLight);//new CGPoint( 0 + offset, _vertMargin * ( i + 1 ) ), new CGPoint( size.Width - offset, _vertMargin * ( i + 1 ) ) 
                _horizLayers[i] = layer;
                layer.StrokeEnd = 0.0f;

                Layer.AddSublayer( layer );
            }

            _vertLayers = new CAShapeLayer[vertLayersNumber];

            for ( int i = 0; i < vertLayersNumber; i++ )
            {
                var layer = LayerUtils.CreateLineLayer(LayoutConstants.BrownLight );//LayoutConstants.BrownLight, new CGPoint( _horizMargin * ( i + 1 ), offset ), new CGPoint( _horizMargin * ( i + 1 ), size.Height - offset )
                _vertLayers[i] = layer;
                layer.StrokeEnd = 0.0f;
                Layer.AddSublayer( layer );
            }
        }
        #endregion

        /// <summary>
        /// Lays out subviews.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The default implementation of this method does nothing on iOS 5.1 and earlier. Otherwise, the default implementation uses any constraints you have set to determine the size and position of any subviews.
        /// </para>
        /// <para>
        /// Subclasses can override this method as needed to perform more precise layout of their subviews. You should override this method only if the autoresizing and constraint-based behaviors of the subviews do not offer the behavior you want. You can use your implementation to set the frame rectangles of your subviews directly.
        /// </para>
        /// <para>
        /// You should not call this method directly. If you want to force a layout update, call the setNeedsLayout method instead to do so prior to the next drawing update. If you want to update the layout of your views immediately, call the layoutIfNeeded method.
        /// </para>
        /// </remarks>
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var offset = 8f;

            for (int i = 0; i < _horizLayers.Length; i++)
            {
                _horizLayers[i].Path = LayerUtils.CreateLineLayerBezierPath(new CGPoint(0 + offset, _vertMargin * (i + 1)), new CGPoint(Bounds.Width - offset, _vertMargin * (i + 1)));
            }

            for (int i = 0; i < _vertLayers.Length; i++)
            {
                _vertLayers[i].Path = LayerUtils.CreateLineLayerBezierPath( new CGPoint( _horizMargin * ( i + 1 ), offset ), new CGPoint( _horizMargin * ( i + 1 ), Bounds.Height - offset ) );
            }

            _vertMargin = (float) Bounds.Width / (_horizLayers.Length + 1);
            _horizMargin = (float) Bounds.Height / (_vertLayers.Length + 1);
        }

        #region Properties
        /// <summary>
        ///     The size of the intrinsic content of the <see cref="T:UIKit.UIView" />.
        /// </summary>
        /// <value>
        ///     The default value is {<see cref="P:UIKit.UIView.NoIntrinsicMetric" />,
        ///     <see cref="P:UIKit.UIView.NoIntrinsicMetric" />}.
        /// </value>
        /// <remarks>
        ///     <para>
        ///         Some views, such as <see cref="T:UIKit.UILabel" />s, have a natural intrinsic size that is not related to the
        ///         actual <see cref="P:UIKit.UIView.Frame" /> that contains the entirety of the <see cref="T:UIKit.UIView" />.
        ///         This property allows the <see cref="T:UIKit.UIView" /> to tell the Auto Layout system what
        ///         <see cref="T:System.Drawing.SizeF" /> it would prefer to be.
        ///     </para>
        ///     <para>
        ///         If a <see cref="T:UIKit.UIView" /> has no intrinsic size, it should return the default {
        ///         <see cref="P:UIKit.UIView.NoIntrinsicMetric" />,<see cref="P:UIKit.UIView.NoIntrinsicMetric" />} value.
        ///     </para>
        /// </remarks>
        //public override CGSize IntrinsicContentSize
        //{
        //    get
        //    {
        //        return new CGSize( Frame.Width, Frame.Height );
        //    }
        //}
        #endregion

        #region Public methods
        public async void AnimateGrid()
        {
            var animation = CABasicAnimation.FromKeyPath( @"strokeEnd" );

            animation.From = FromObject( (nfloat)0.0 );
            animation.To = FromObject( (nfloat)1.0 );
            animation.RemovedOnCompletion = false;
            animation.Duration = 0.4f;
            animation.TimingFunction = CAMediaTimingFunction.FromName( CAMediaTimingFunction.EaseInEaseOut );

            for ( int i = 0; i < _horizLayers.Length; i++ )
            {
                _horizLayers[i].StrokeEnd = 1.0f;
                _horizLayers[i].AddAnimation( animation, animation.KeyPath );
            }

            await Task.Delay( 400 );

            for ( int i = 0; i < _vertLayers.Length; i++ )
            {
                _vertLayers[i].StrokeEnd = 1.0f;
                _vertLayers[i].AddAnimation( animation, animation.KeyPath );
            }
        }

        public GridCoordinate GetTouchGridCoordinate( CGPoint coord )
        {
            var xCoord = (int)( coord.X / _vertMargin );
            var yCoord = (int)( coord.Y / _horizMargin );

            return new GridCoordinate( xCoord, yCoord );
        }

        public void SetMark( GridCoordinate coordinate, IMark mark )
        {
            UIView view = null;
            if ( mark is CrossMark )
            {
                view = new CrossView();
                ( (CrossView)view ).Animate();
            }

            if ( mark is NoughtMark )
            {
                view = new NoughtView();
                ( (NoughtView)view ).Animate();
            }

            if ( view == null )
            {
                return;
            }

            view.Frame = new RectangleF( coordinate.X * _horizMargin, coordinate.Y * _vertMargin, _horizMargin, _vertMargin );

            AddSubview( view );
        }

        public void WinningLine( GridCoordinate from, GridCoordinate to )
        {
            CAShapeLayer line = new CAShapeLayer();

            line.Path = LayerUtils.CreateLineLayerBezierPath( new CGPoint( from.X * _horizMargin + _horizMargin / 2, from.Y * _vertMargin + _vertMargin / 2 ),
                                                              new CGPoint( to.X * _horizMargin + _horizMargin / 2, to.Y * _vertMargin + _vertMargin / 2 ) );
            line.StrokeColor = LayoutConstants.MagentaColor.CGColor;
            line.LineWidth = 10f;
            line.LineCap = CAShapeLayer.CapRound;

            Layer.AddSublayer( line );

            var animation = CABasicAnimation.FromKeyPath( @"strokeEnd" );

            animation.From = FromObject( (nfloat)0.0 );
            animation.To = FromObject( (nfloat)1.0 );
            animation.RemovedOnCompletion = false;
            animation.Duration = 0.4f;
            animation.TimingFunction = CAMediaTimingFunction.FromName( CAMediaTimingFunction.EaseInEaseOut );

            // _horizLayers[i].StrokeEnd = 1.0f;
            line.AddAnimation( animation, animation.KeyPath );
        }
        #endregion
    }
}
