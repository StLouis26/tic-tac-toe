using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace TicTacToe.UI
{
    internal static class LayerUtils
    {
        #region Public methods
        public static CAShapeLayer CreateLineLayer( UIColor color )
        {
            var layer = new CAShapeLayer();

            //layer.FillColor = UIColor.Blue.CGColor;
            layer.StrokeColor = color.CGColor;
            layer.LineCap = CAShapeLayer.CapRound;
            layer.LineWidth = 10;

            return layer;
        }

        public static CGPath CreateLineLayerBezierPath( CGPoint from, CGPoint to )
        {
            var path = new UIBezierPath();
            path.MoveTo( from );
            path.AddLineTo( to );
            return path.CGPath;
        }
        #endregion
    }
}
