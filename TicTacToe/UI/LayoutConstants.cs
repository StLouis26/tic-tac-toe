using UIKit;

namespace TicTacToe.UI
{
    internal static class LayoutConstants
    {
        #region Constants
        public static UIColor BlueColor = UIColor.FromRGB( 87, 181, 224 );
        public static UIColor YellowColor = UIColor.FromRGB( 255, 213, 0 );
        public static UIColor BrownLight = UIColor.FromRGB( 218, 183, 84 );
        public static UIColor BrownUltraLight = UIColor.FromRGB( 255, 244, 214 );
        public static UIColor GreenColor = UIColor.FromRGB( 145, 255, 0 );
        public static UIColor MagentaColor = UIColor.FromRGB( 151, 0, 71 );
        #endregion
    }
}
