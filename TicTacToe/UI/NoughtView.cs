using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace TicTacToe.UI
{
    internal class NoughtView : UIView
    {
        #region Public methods
        [Export( "layerClass" )]
        public static Class LayerClass()
        {
            return new Class( typeof ( CAShapeLayer ) );
        }

        /// <summary>
        ///     Lays out subviews.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         The default implementation of this method does nothing on iOS 5.1 and earlier. Otherwise, the default
        ///         implementation uses any constraints you have set to determine the size and position of any subviews.
        ///     </para>
        ///     <para>
        ///         Subclasses can override this method as needed to perform more precise layout of their subviews. You should
        ///         override this method only if the autoresizing and constraint-based behaviors of the subviews do not offer the
        ///         behavior you want. You can use your implementation to set the frame rectangles of your subviews directly.
        ///     </para>
        ///     <para>
        ///         You should not call this method directly. If you want to force a layout update, call the setNeedsLayout method
        ///         instead to do so prior to the next drawing update. If you want to update the layout of your views immediately,
        ///         call the layoutIfNeeded method.
        ///     </para>
        /// </remarks>
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var shapeLayer = (CAShapeLayer)Layer;

            var inset = 11f;

            shapeLayer.Path =
                UIBezierPath.FromRoundedRect( new CGRect( Bounds.X + inset, Bounds.Y + inset, Bounds.Width - 2 * inset, Bounds.Height - 2 * inset ), ( Bounds.Width - 2 * inset ) / 2 ).CGPath;

            shapeLayer.FillColor = UIColor.Clear.CGColor;
            shapeLayer.StrokeColor = LayoutConstants.BlueColor.CGColor;
            shapeLayer.LineWidth = 3f;
        }

        public void Animate()
        {
            var animation = CABasicAnimation.FromKeyPath( @"strokeEnd" );

            animation.From = FromObject( (nfloat)0.0 );
            animation.To = FromObject( (nfloat)1.0 );
            animation.RemovedOnCompletion = false;
            animation.Duration = 0.4f;
            animation.TimingFunction = CAMediaTimingFunction.FromName( CAMediaTimingFunction.EaseInEaseOut );

            // _horizLayers[i].StrokeEnd = 1.0f;
            Layer.AddAnimation( animation, animation.KeyPath );
        }
        #endregion
    }
}
