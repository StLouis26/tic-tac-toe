using System;
using UIKit;

namespace TicTacToe.UI
{
    public class GradientViewController : UIViewController
    {
        #region Constructors
        protected GradientViewController( IntPtr intPtr ) : base( intPtr )
        {
        }
        #endregion

        #region Public methods
        /// <summary>
        ///     Initializes the <see cref="P:UIKit.UIViewController.View" /> property.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method should not be called directly. It is called when the <see cref="P:UIKit.UIViewController.View" />
        ///         property is accessed and lazily initialized. Generally, the appropriate <see cref="T:UIKit.UIView" /> will be
        ///         loaded from a nib file, but application developers may override it to create a custom
        ///         <see cref="T:UIKit.UIView" />. This method should not be overridden to provide general initialization on
        ///         loading a view, that belongs in the <see cref="M:UIKit.UIViewController.ViewDidLoad" /> method.
        ///     </para>
        ///     <block subset="none" type="overrides">
        ///         <list type="bullet">
        ///             <item>
        ///                 <term>
        ///                     Overriders who create a custom <see cref="T:UIKit.UIView" /> should not call
        ///                     <c>base.LoadView()</c>.
        ///                 </term>
        ///             </item>
        ///             <item>
        ///                 <term>
        ///                     Overriders should assign the custom <see cref="T:UIKit.UIView" /> to the
        ///                     <see cref="P:UIKit.UIViewController.View" /> property.
        ///                 </term>
        ///             </item>
        ///             <item>
        ///                 <term>
        ///                     The <see cref="T:UIKit.UIView" />s created by this method must not be shared with other
        ///                     <see cref="T:UIKit.UIViewController" />s.
        ///                 </term>
        ///             </item>
        ///         </list>
        ///     </block>
        /// </remarks>
        public override void LoadView()
        {
            View = new BackgroundGradientView();
        }
        #endregion
    }
}
