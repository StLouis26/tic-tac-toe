using System;
using UIKit;

namespace TicTacToe
{
    internal static class LayoutUtils
    {
        #region Public methods
        public static void SetContstraintsMatchParent( UIView containerView, UIView newSubview )
        {
            AddEqualConstraint( containerView, newSubview, NSLayoutAttribute.Top );
            AddEqualConstraint( containerView, newSubview, NSLayoutAttribute.Leading );
            AddEqualConstraint( containerView, newSubview, NSLayoutAttribute.Bottom );
            AddEqualConstraint( containerView, newSubview, NSLayoutAttribute.Trailing );
        }

        public static void AddEqualConstraint( UIView containerView, UIView newSubview, NSLayoutAttribute attribute )
        {
            containerView.AddConstraint( NSLayoutConstraint.Create( newSubview, attribute, NSLayoutRelation.Equal, containerView, attribute, (nfloat)1.0, (nfloat)0.0 ) );
        }

        public static void CenterView( UIView containerView, UIView newSubview )
        {
            CenterVertically( containerView, newSubview );
            CenterHorizontally( containerView, newSubview );
        }

        public static void CenterVertically( UIView containerView, UIView newSubview )
        {
            var constraint2 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterY, (nfloat)1.0, (nfloat)0.0 );
            containerView.AddConstraint( constraint2 );
        }

        public static void CenterHorizontally( UIView containerView, UIView newSubview )
        {
            var constraint1 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterX, (nfloat)1.0, (nfloat)0.0 );
            containerView.AddConstraint( constraint1 );
        }

        public static void TopPosition( UIView containerView, UIView newSubview, nfloat position )
        {
            var constraint1 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.Top, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Top, (nfloat)1.0, position );
            containerView.AddConstraint( constraint1 );
        }

        public static void TopPosition( UIView containerView, UIView newSubview, UIView otherView, nfloat position )
        {
            var constraint1 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.Top, NSLayoutRelation.Equal, otherView, NSLayoutAttribute.Bottom, (nfloat)1.0, position );
            containerView.AddConstraint( constraint1 );
        }

        public static void SetWidth( UIView containerView, UIView newSubview, nfloat width )
        {
            var constraint1 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, (nfloat)1.0, width );
            containerView.AddConstraint( constraint1 );
        }

        public static void SetHeight( UIView containerView, UIView newSubview, nfloat height )
        {
            var constraint1 = NSLayoutConstraint.Create( newSubview, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.Height, (nfloat)1.0, height );
            containerView.AddConstraint( constraint1 );
        }

        public static UIFont GetAppFont( nfloat size )
        {
            return UIFont.FromName( "Helvetica", size );
        }

        public static UIFont GetAppFontBold( nfloat size )
        {
            return UIFont.FromName( "Helvetica-Bold", size );
        }
        #endregion
    }
}
