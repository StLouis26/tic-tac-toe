using System.Collections.Generic;
using TicTacToe.Players;

namespace TicTacToe.Game
{
    internal interface IGame
    {
        #region Properties
        IGrid Grid
        {
            get;
        }

        int NumberOfPlayers
        {
            get;
        }

        IReadOnlyCollection<IPlayer> Players
        {
            get;
        }

        bool IsStarted
        {
            get;
        }

        bool IsFinished
        {
            get;
        }

        IPlayer CurrentPlayer
        {
            get;
        }
        #endregion

        #region Public methods
        void StartGame();
        ActionResult CellClick( int x, int y );
        #endregion
    }
}
