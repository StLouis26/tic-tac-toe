using TicTacToe.Marks;

namespace TicTacToe.Game
{
    internal interface IGrid
    {
        #region Properties
        int Rows
        {
            get;
        }

        int Columns
        {
            get;
        }
        #endregion

        #region Public methods
        IMark GetMark( int x, int y );

        /// <summary>
        /// </summary>
        /// <param name="mark"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>true if allowed</returns>
        bool SetMark( IMark mark, int x, int y );
        #endregion
    }
}
