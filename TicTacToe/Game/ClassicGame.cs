using System;
using System.Collections.Generic;
using TicTacToe.Game.ResultChecker;
using TicTacToe.Players;

namespace TicTacToe.Game
{
    internal class ClassicGame : IGame
    {
        #region Fields
        private IPlayer[] _players;
        private IGameResultChecker _resultChecker;
        #endregion

        #region Constructors
        public ClassicGame( IPlayer player1, IPlayer player2 )
        {
            if ( player1 == null || player2 == null )
            {
                throw new ArgumentNullException();
            }

            _players = new IPlayer[NumberOfPlayers];
            Grid = new GameGrid( 3, 3 );

            _players[0] = player1;
            _players[1] = player2;

            _resultChecker = new LineGameResultChecker( 3 );
        }
        #endregion

        #region Properties
        public IGrid Grid
        {
            get;
            private set;
        }

        public int NumberOfPlayers
        {
            get
            {
                return 2;
            }
        }

        public IReadOnlyCollection<IPlayer> Players
        {
            get
            {
                return _players;
            }
        }

        public bool IsStarted
        {
            get;
            private set;
        }

        public bool IsFinished
        {
            get;
            private set;
        }

        public IPlayer CurrentPlayer
        {
            get;
            private set;
        }
        #endregion

        #region Public methods
        public void StartGame()
        {
            CurrentPlayer = _players[0];
            IsStarted = true;
        }

        public ActionResult CellClick( int x, int y )
        {
            CheckGameIsFinished();
            var allowed = Grid.SetMark( CurrentPlayer.Mark, x, y );

            if ( !allowed )
            {
                return new ActionResult( false, true, null );
            }

            var checkResult = _resultChecker.Check( Grid, x, y );

            if ( !checkResult.IsFinished )
            {
                for ( int i = 0; i < _players.Length; i++ )
                {
                    if ( _players[i] == CurrentPlayer )
                    {
                        var nextPayerId = ( i + 1 ) % _players.Length;
                        CurrentPlayer = _players[nextPayerId];
                        break;
                    }
                }
            }

            return new ActionResult( checkResult.IsFinished, false, checkResult );
        }
        #endregion

        #region Private methods
        private void CheckGameIsFinished()
        {
            if ( IsFinished )
            {
                throw new InvalidOperationException( "Game is finished" );
            }
        }
        #endregion
    }
}
