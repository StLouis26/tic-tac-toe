using System;
using TicTacToe.Marks;

namespace TicTacToe.Game.ResultChecker
{
    internal class LineGameResultChecker : IGameResultChecker
    {
        #region SearchOperation enum
        public enum SearchOperation
        {
            Add,
            Subsctract,
            None
        }
        #endregion

        #region Fields
        private int _marksInLineNumber;
        #endregion
        
        #region Constructors
        /// <summary>
        /// </summary>
        /// <param name="marksInLineNumber">Number of marks in line to win</param>
        public LineGameResultChecker( int marksInLineNumber )
        {
            _marksInLineNumber = marksInLineNumber;
        }
        #endregion

        #region Public methods
        public CheckResult Check( IGrid grid, int x, int y )
        {
            if ( grid.Rows != 3 || grid.Columns != 3 )
            {
                throw new ArgumentException( "Invalid grid for classica game checker" );
            }

            var operations = Enum.GetValues( typeof ( SearchOperation ) );

            var mark = grid.GetMark( x, y );
            if ( !mark.IsEmptyMark )
            {
                var result = StartSearchRecursion( grid, x, y, mark, operations );
                if ( result != null && result.IsFinished )
                {
                    return result;
                }
            }

            for ( int i = 0; i < grid.Rows; i++ )
            {
                for ( int j = 0; j < grid.Columns; j++ )
                {
                    if ( grid.GetMark( i, j ).IsEmptyMark )
                    {
                        return new CheckResult( false );
                    }
                }
            }

            return new CheckResult( true );
        }

        /// <summary>
        /// </summary>
        /// <param name="alreadyInRow"></param>
        /// <returns>markId if WIN, otherwise -1</returns>
        public int SearchOperationRecursion( IGrid grid, int alreadyInRow, int markId, int x, int y, SearchOperation horizontalSearchType, SearchOperation verticalSearchType )
        {
            if ( !( x >= 0 && x < grid.Columns && y >= 0 && y < grid.Rows ) )
            {
                return -1;
            }

            var mark = grid.GetMark( x, y );

            if ( mark.IsEmptyMark )
            {
                return -1;
            }

            if ( mark.PlayerGroupId == markId )
            {
                if ( alreadyInRow + 1 == _marksInLineNumber )
                {
                    return markId;
                }

                return SearchOperationRecursion( grid,
                                                 alreadyInRow + 1,
                                                 mark.PlayerGroupId,
                                                 ModifyCoordinate( x, horizontalSearchType ),
                                                 ModifyCoordinate( y, verticalSearchType ),
                                                 horizontalSearchType,
                                                 verticalSearchType );
            }

            return -1;
        }

        public int ModifyCoordinate( int coord, SearchOperation operation, int value = 1 )
        {
            switch ( operation )
            {
                case SearchOperation.Add:
                    return coord + value;
                case SearchOperation.Subsctract:
                    return coord - value;
            }
            return coord;
        }
        #endregion

        #region Private methods
        private CheckResult StartSearchRecursion( IGrid grid, int x, int y, IMark mark, Array searchOperations )
        {
            foreach ( SearchOperation horizSearchOperation in searchOperations )
            {
                foreach ( SearchOperation vertSearchOperation in searchOperations )
                {
                    if ( vertSearchOperation == SearchOperation.None && horizSearchOperation == SearchOperation.None )
                    {
                        continue;
                    }

                    var result = SearchOperationRecursion( grid, 0, mark.PlayerGroupId, x, y, horizSearchOperation, vertSearchOperation );
                    if ( result >= 0 )
                    {
                        return new CheckResult( result,
                                                new GridCoordinate( x, y ),
                                                new GridCoordinate( ModifyCoordinate( x, horizSearchOperation, _marksInLineNumber - 1 ),
                                                                    ModifyCoordinate( y, vertSearchOperation, _marksInLineNumber - 1 ) ) );
                    }
                }
            }

            return null;
        }
        #endregion
    }
}
