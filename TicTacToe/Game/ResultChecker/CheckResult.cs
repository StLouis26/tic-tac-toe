namespace TicTacToe.Game.ResultChecker
{
    public class CheckResult
    {
        #region Constructors
        public CheckResult()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public CheckResult( int winningGropId, GridCoordinate beginningWinGridCoordinate, GridCoordinate endingWinGridCoordinate )
        {
            WinningGropId = winningGropId;
            BeginningWinGridCoordinate = beginningWinGridCoordinate;
            EndingWinGridCoordinate = endingWinGridCoordinate;
            IsWin = true;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public CheckResult( bool isDraw )
        {
            IsDraw = isDraw;
        }
        #endregion

        #region Properties
        public int WinningGropId
        {
            get;
            private set;
        }

        public GridCoordinate BeginningWinGridCoordinate
        {
            get;
            private set;
        }

        public GridCoordinate EndingWinGridCoordinate
        {
            get;
            private set;
        }

        public bool IsDraw
        {
            get;
            private set;
        }

        public bool IsWin
        {
            get;
            set;
        }

        public bool IsFinished
        {
            get
            {
                return IsWin || IsDraw;
            }
        }
        #endregion
    }
}
