namespace TicTacToe.Game.ResultChecker
{
    public class GridCoordinate
    {
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public GridCoordinate( int x, int y )
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Properties
        public int X
        {
            get;
            private set;
        }

        public int Y
        {
            get;
            private set;
        }
        #endregion
    }
}
