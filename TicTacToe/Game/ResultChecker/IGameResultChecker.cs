using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace TicTacToe.Game.ResultChecker
{
    interface IGameResultChecker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Winning GroupId if game finished, -1 if game is not finished yet</returns>
        CheckResult Check( IGrid grid, int x, int y );
    }
}