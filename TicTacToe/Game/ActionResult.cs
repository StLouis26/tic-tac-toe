using TicTacToe.Game.ResultChecker;

namespace TicTacToe.Game
{
    public class ActionResult
    {
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public ActionResult( bool gameIsFinished, bool notAllowed, CheckResult checkResult )
        {
            GameIsFinished = gameIsFinished;
            NotAllowed = notAllowed;
            CheckResult = checkResult;
        }

        public ActionResult()
        {
        }
        #endregion

        #region Properties
        public bool GameIsFinished
        {
            get;
            private set;
        }

        /// <summary>
        ///     Ex. Field is checked already
        /// </summary>
        public bool NotAllowed
        {
            get;
            private set;
        }

        public CheckResult CheckResult
        {
            get;
            private set;
        }
        #endregion
    }
}
