using System;
using TicTacToe.Marks;

namespace TicTacToe.Game
{
    internal class GameGrid : IGrid
    {
        #region Fields
        private IMark[,] _grid;
        #endregion

        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public GameGrid( int rows, int columns )
        {
            if ( rows < 3 || columns < 3 )
            {
                throw new ArgumentException();
            }

            Rows = rows;
            Columns = columns;

            _grid = new IMark[Rows, Columns];

            for ( int i = 0; i < Rows; i++ )
            {
                for ( int j = 0; j < Columns; j++ )
                {
                    _grid[i, j] = new EmptyMark();
                }
            }
        }
        #endregion

        #region Properties
        public int Rows
        {
            get;
            private set;
        }

        public int Columns
        {
            get;
            private set;
        }
        #endregion

        #region Public methods
        public IMark GetMark( int x, int y )
        {
            CheckCoordinates( x, y );

            return _grid[x, y];
        }

        /// <summary>
        /// </summary>
        /// <param name="mark"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>true if allowed</returns>
        public bool SetMark( IMark mark, int x, int y )
        {
            CheckCoordinates( x, y );

            if ( _grid[x, y].IsEmptyMark )
            {
                _grid[x, y] = mark;
                return true;
            }

            return false;
        }
        #endregion

        #region Private methods
        private void CheckCoordinates( int x, int y )
        {
            if ( x < 0 || x >= Columns )
            {
                throw new ArgumentException();
            }

            if ( y < 0 || y >= Rows )
            {
                throw new ArgumentException();
            }
        }
        #endregion
    }
}
